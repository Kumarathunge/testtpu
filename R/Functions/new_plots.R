
#-----------------------------------------------------------------------------------------------------------------
#-----------------------------------------------------------------------------------------------------------------

#plot TPU:Vcmax ratio

# fit temperature response of TPU limitation 
# Only mature trees in native environments

tfits<-read.csv("aci_fits.csv")

# get curves only show TPU limitation
fits_global_tpu<-subset(tfits,!is.na(TPU)) 

# Get best fitted curves (Best fit if R2>=0.90; My definition)
# params_global_clean<-subset(fits_global_tpu,fits_global_tpu$Ci_t2_tpu>400.0001 & fits_global_tpu$R2_TPU>=0.9)

params_global_clean<-subset(fits_global_tpu,fits_global_tpu$Ci_t2_tpu>400.0001 & fits_global_tpu$R2_TPU>=0.9)
params_global_clean$TV_ratio<-with(params_global_clean,TPU/Vcmax)
params_global_clean$TJ_ratio<-with(params_global_clean,TPU/Jmax)

params_global_clean$TleafFac <- cut(params_global_clean$Ts,breaks=seq(5,50,1),labels=1:45)
col_codes<-read.csv("color_codes.csv")
params_global_clean<-merge(params_global_clean,col_codes,by="PFT")
params_global_clean<-subset(params_global_clean,!is.na(PFT))



dat1<-summaryBy(.~PFT+TleafFac,FUN=mean,data=subset(params_global_clean,!is.na(PFT)),keep.names=T,na.rm=T)

xlimit<-c(0,50)
ylimit<-c(0,0.4)



pdf("tpu_vcmax_ratio.pdf",width = 5, height = 8)
par(mar=c(4,5,0.5,0.5),oma=c(0,0,0,0),mfrow=c(2,1))


#-- Ci transition at Vcmax to Jmax limitation

plot(TV_ratio~Ts,data=params_global_clean,legend=F,las=1,xlim=xlimit,ylim=ylimit,cex.lab=2,xaxt="n",yaxt="n",
     ylab="",col=alpha(COL,0.4)[params_global_clean$colorcode],pch=params_global_clean$pchcode,cex=0.5,
     xlab="",axes=F)

points(TV_ratio~Ts,data=dat1,pch=dat1$pchcode,cex=0.6,col="black",bg=alpha(COL,1)[dat1$colorcode],
       axes=F,xlim=xlimit,ylim=ylimit)

magaxis(side=c(1,2,4),labels=c(1,1,0),frame.plot=T,las=1,cex.axis=1.1,ratio=0.4,tcl=0.2)

legend("topright",paste("(",letters[1],")",sep=""),bty="n",cex=1,text.font=2)


# fit linear regression models

arctic<-summary(lm(TV_ratio~Ts,data=subset(params_global_clean,params_global_clean$PFT=="ARCTIC")))
ablineclip(arctic$coefficients[1],arctic$coefficients[2],x1=5,x2=25,lty=2,col=COL[1],lwd=3)

decid<-summary(lm(TV_ratio~Ts,data=subset(params_global_clean,params_global_clean$PFT=="BDT_TE")))
ablineclip(decid$coefficients[1],decid$coefficients[2],x1=5,x2=45,lty=2,col=COL[2],lwd=3)

egte<-summary(lm(TV_ratio~Ts,data=subset(params_global_clean,params_global_clean$PFT=="BET_TE")))
ablineclip(egte$coefficients[1],egte$coefficients[2],x1=5,x2=45,lty=2,col=COL[3],lwd=3)


trop<-summary(lm(TV_ratio~Ts,data=subset(params_global_clean,params_global_clean$PFT=="BET_Tr")))
ablineclip(trop$coefficients[1],trop$coefficients[2],x1=18,x2=45,lty=2,col=COL[4],lwd=3)

nbr<-summary(lm(TV_ratio~Ts,data=subset(params_global_clean,params_global_clean$PFT=="NET_B")))
ablineclip(nbr$coefficients[1],nbr$coefficients[2],x1=5,x2=40,lty=2,col=COL[5],lwd=3)

nte<-summary(lm(TV_ratio~Ts,data=subset(params_global_clean,params_global_clean$PFT=="NET_TE")))
ablineclip(nte$coefficients[1],nte$coefficients[2],x1=5,x2=40,lty=2,col=COL[6],lwd=3)


title(xlab=expression(Leaf~Temperature~(degree*C)),cex.lab=1.2,line=2.5,outer=F)
title(ylab=expression(TPU:V[cmax]),cex.lab=1.2,line=3,outer=F)

legend("topleft",legend=levels(unique(dat1$PFT)),pt.bg=alpha(COL,1)[c(1:6)],
       pch=c(21,22,24,24,25,25),bty="n",ncol=3,cex=0.6)

#------------------------------------------------------------------------------------------------------------
#------------------------------------------------------------------------------------------------------------

# fit temperature response by PFT

tofit<-split(mature_data,mature_data$PFT)
fitted_p<-lapply(tofit[2:6],function(x)fitpeaked(x,return="Peak"))
fitted_p<-data.frame(do.call(rbind,list(fitted_p[[1]],fitted_p[[2]],fitted_p[[3]],fitted_p[[4]],fitted_p[[5]])))
fitted_p$PFT<-names(tofit[2:6])


arctic<-data.frame(do.call(rbind,list(lapply(tofit[1],function(x)fitpeaked(x,return="Arr"))[[1]]))) # cannot fit Peak model
arctic$PFT<-"ARCTIC"

fitted_p<-rbind.fill(fitted_p,arctic)


toplot2<-subset(params_global_clean,params_global_clean$Ts>18 & params_global_clean$Ts<21.5)
toplot3<-summaryBy(TV_ratio~PFT,data=toplot2,FUN=c(mean,std.error))
toplot3$CIL_R<-with(toplot3,TV_ratio.mean-1.96*TV_ratio.std.error)
toplot3$CIU_R<-with(toplot3,TV_ratio.mean+1.96*TV_ratio.std.error)

barplot2(toplot3$TV_ratio.mean,plot.ci=T,ci.l=toplot3$CIL_R,ci.u=toplot3$CIU_R,ci.width=0.2,
         names.arg = unique(toplot3$PFT),ylim=c(0,0.25),axes=F,xlab="",ylab="",cex.names=.7)

magaxis(side=c(2,4),labels=c(1,0),frame.plot=T,las=1,cex.axis=1.1,ratio=0.4,tcl=0.2,majorn = 4)
legend("topright",paste("(",letters[2],")",sep=""),bty="n",cex=1,text.font=2)

title(ylab=expression(TPU:V[cmax]~at~20~degree*C),cex.lab=1.2,line=3,outer=F)
title(xlab=expression(Plant~Functional~Type),cex.lab=1.2,line=3,outer=F)

dev.off()



#------------------------------------------------------------------------------------------------------------
#------------------------------------------------------------------------------------------------------------

# plot 
# get a subset of dataset: (mature trees in native environments) 18 dataset


tfits<-read.csv("aci_fits.csv")

# get curves only show TPU limitation
fits_global_tpu<-subset(tfits,!is.na(TPU)) 

# Get best fitted curves (Best fit if R2>=0.90; My definition)
params_global_clean<-subset(fits_global_tpu, fits_global_tpu$R2_TPU>=0.8)
col_codes<-read.csv("color_codes.csv")
params_global_clean<-merge(params_global_clean,col_codes,by="PFT")



mature_data_new<-subset(params_global_clean,  Dataset %in% c("Semi-arid Woodland, AU-WA","Rainforest, Brazil",
                                                             "Maritime Pine, France","Rainforest, Au-QLD",
                                                             "Scots Pine, Sweden","Tundra USA-AK","Black Spruce, USA-MN","Mongolian Oak, Japan",
                                                             "Savanna Eucalypt, AU-NT","Eucalypt Woodland, AU-NSW",
                                                             "Loblolly Pine, US-NC","Rainforest understorey, Puerto Rico","Rainforest, Panama (A)",
                                                             "Scots Pine, Finland","Subalpine Eucalypt, AU-NSW (A)")) 

mature_data_new$Dataset<-factor(mature_data_new$Dataset)

tofit1<-split(mature_data_new,mature_data_new$Dataset)

fitted_p_new<-get_topts(lapply(tofit1[c(1,2,4,6,7:13)],function(x)fitpeaked(x,return="Peak"))) # this give error message on singular gradient. But fits all datasets
fitted_p_new$Dataset<-names(tofit1[c(1,2,4,6,7:13)])

fitted_p_new2<-get_topts(lapply(tofit1[c(3,5,14,15)],function(x)fitarr(x))) # this give error message on singular gradient. But fits all datasets
fitted_p_new2$Dataset<-names(tofit1[c(3,5,14,15)])


fitted_p_new<-rbind.fill(fitted_p_new,fitted_p_new2)
met_dat<-mature_data_new[c(22,39,1,70:75)]

toplot3<-merge(fitted_p_new,met_dat,by="Dataset")

toplot3 <- toplot3[!duplicated(toplot3[,c("Dataset")]),]

toplot3$TV_ratio<-with(toplot3,Vpmax25/Vcmax_TPU)


# toplot3<-subset(mature_data,mature_data$Ts>23.5 & mature_data$Ts<26.5)


pdf("tpu_thome.pdf",width = 5, height = 8)
par(mar=c(1,0.5,0.5,0.5),oma=c(3,3,0,0),mfrow=c(2,1))


with(toplot3,plot(mGDD0,Vpmax25,bg=alpha(COL,0.9)[toplot3$colorcode],pch=toplot3$pchcode,cex=.1,axes=F,xlab="",ylab="",
                                                        xlim=c(0,30),ylim=c(0,25)))

magaxis(side=c(1,2,4),labels=c(1,1,0),frame.plot=T,las=1,cex.axis=1.1,ratio=0.4,tcl=0.2,majorn = 4)

adderrorbars(toplot3$mGDD0,toplot3$Vpmax25,toplot3$Vpmax25.se,"updown",col=alpha(COL,0.9)[toplot3$colorcode])

with(toplot3,points(mGDD0,Vpmax25,bg=alpha(COL,0.9)[toplot3$colorcode],pch=toplot3$pchcode,cex=1.2,axes=F,xlab="",ylab="",
                  xlim=c(0,30),ylim=c(0,25),col=alpha(COL,0.9)[toplot3$colorcode]))

title(ylab=expression(V[pmax25]~(mu*mol~m^-2~s^-1)),cex.lab=1.2,line=1.5,outer=T,adj=0.85)
title(xlab=expression(mGDD0~(degree*C)),cex.lab=1.2,line=1.5,outer=T,adj=.525)

par(new=T)
smoothplot(mGDD0,Vpmax25, kgam=3,linecols=alpha("black",0.5),
           
           polycolor=alpha("lightgray",0.3),
           linecol=c("black"),pointcols=NA,
           cex=1,main="",
           xlim=c(0,30),ylim=c(0,25),xlab="",ylab="",
           data=toplot3, axes=F)


legend("topright",legend=levels(unique(toplot3$PFT)),pt.bg=alpha(COL,1)[c(1:6)],
       pch=c(21,22,24,24,25,25),bty="n",ncol=2,cex=.8)

legend("topleft",paste("(",letters[1],")",sep=""),bty="n",cex=1,text.font=2)


# fitted linear regression plot

lm_thome<-lm(Vpmax25~mGDD0,data=toplot3)
lmod<-summary(lm_thome)
# 
# mylabel_1 = bquote(italic(r)^2 == .(format(lmod$adj.r.squared, digits = 2)))
# mylabel_2 = bquote(italic(y) == .(format(lmod$coefficients[2], digits = 2))~italic(x) + .(format(lmod$coefficients[1], digits = 2)))
# 
# 
# text(x = 5, y = 2.2, labels = mylabel_2,cex=1)
# text(x = 5, y = 1, labels = mylabel_1,cex=1)
# 

#-----------------------------------------------------
#-----------------------------------------------------

# ratio plot 

mature_data_new$TV_ratio<-with(mature_data_new,TPU/Vcmax)

toplot4<-summaryBy(TV_ratio+mGDD0+colorcode+pchcode~Dataset+PFT,data=subset(mature_data_new,mature_data_new$Ts>19 & mature_data_new$Ts<27),
                                                                            FUN=c(mean,std.error))

with(toplot4,plot(mGDD0.mean,TV_ratio.mean,bg=alpha(COL,0.9)[toplot4$colorcode.mean],pch=toplot4$pchcode.mean,cex=.1,axes=F,xlab="",ylab="",
                  xlim=c(0,30),ylim=c(0,.3)))
magaxis(side=c(1,2,4),labels=c(1,1,0),frame.plot=T,las=1,cex.axis=1.1,ratio=0.4,tcl=0.2,majorn = 4)


adderrorbars(toplot4$mGDD0.mean,toplot4$TV_ratio.mean,toplot4$TV_ratio.std.error,"updown",col=alpha(COL,0.9)[toplot4$colorcode.mean])

with(toplot4,points(mGDD0.mean,TV_ratio.mean,bg=alpha(COL,0.9)[toplot4$colorcode.mean],pch=toplot4$pchcode.mean,cex=1.2,axes=F,xlab="",ylab="",
                  xlim=c(0,30),ylim=c(0,.3),col=alpha(COL,0.9)[toplot4$colorcode.mean]))


par(new=T)
smoothplot(mGDD0.mean,TV_ratio.mean, kgam=3,linecols=alpha("black",0.5),
           
           polycolor=alpha("lightgray",0.3),
           linecol=c("black"),pointcols=NA,
           cex=1,main="",
           xlim=c(0,30),ylim=c(0,.3),xlab="",ylab="",
           data=toplot4, axes=F)

title(ylab=expression(V[pmax25]:V[cmax25]),cex.lab=1.2,line=1.5,outer=T,adj=0.15)


legend("topleft",paste("(",letters[2],")",sep=""),bty="n",cex=1,text.font=2)

# rug(0.167,lwd=3,line=-0.5,side=2)

# fitted linear regression plot

lm_thome1<-lm(TV_ratio.mean~mGDD0.mean,data=toplot4,weights=1/toplot4$TV_ratio.std.error)
lmod1<-summary(lm_thome1)

# mylabel_1 = bquote(italic(r)^2 == .(format(lmod1$adj.r.squared, digits = 2)))
# mylabel_2 = bquote(italic(y) == .(format(lmod1$coefficients[2], digits = 2))~italic(x) + .(format(lmod1$coefficients[1], digits = 2)))
# 
# 
# text(x = 5, y = 0.03, labels = mylabel_2,cex=1)
# text(x = 5, y = 0.01, labels = mylabel_1,cex=1)
# 


dev.off()



#-------------------------------------------------------------------------------------------------------------------------------------------
#-------------------------------------------------------------------------------------------------------------------------------------------

#-------------------------------------------------------------------------------------------------------------------
#-------------------------------------------------------------------------------------------------------------------

# fit temperature response of TPU limitation 
# Only mature trees in native environments

tfits<-read.csv("aci_fits.csv")

# get curves only show TPU limitation
fits_global_tpu<-subset(tfits,!is.na(TPU)) 

# Get best fitted curves (Best fit if R2>=0.90; My definition)
params_global_clean<-subset(fits_global_tpu, fits_global_tpu$R2_TPU>=0.9)




# get a subset of dataset: (mature trees in native environments) 18 dataset

mature_data<-subset(params_global_clean,  Dataset %in% c("Semi-arid Woodland, AU-WA","Rainforest, Brazil","Norway Spruce, Sweden",
                                                         "Maritime Pine, France","Rainforest, Au-QLD","Red Pine, Japan",
                                                         "Scots Pine, Sweden","Tundra USA-AK","Black Spruce, USA-MN","Mongolian Oak, Japan",
                                                         "Savanna Eucalypt, AU-NT","Eucalypt Woodland, AU-NSW","Hinoki Cypress, Japan",
                                                         "Loblolly Pine, US-NC","Rainforest understorey, Puerto Rico","Rainforest, Panama (A)",
                                                         "Scots Pine, Finland","Subalpine Eucalypt, AU-NSW (A)")) #these dataset are from


mature_data$Dataset<-factor(mature_data$Dataset)



fitpeaked(tofit[[2]],return="Peak",start=list(k25=8, Ea=10, delS = 0.64))
#-----------------------------------------------------------------------------------------------------------------
#-----------------------------------------------------------------------------------------------------------------

# fit temperature response of TPU limitation

#-----------------------------------------------------------------------------------------------------------------
#-----------------------------------------------------------------------------------------------------------------


tofit<-split(mature_data,mature_data$PFT)
fitted_p<-get_topts(lapply(tofit[3:6],function(x)fitpeaked(x,return="Peak")))
# fitted_p<-data.frame(do.call(rbind,list(fitted_p[[1]],fitted_p[[2]],fitted_p[[3]],fitted_p[[4]],fitted_p[[5]])))
fitted_p$PFT<-names(tofit[c(3:6)])

fitted_a<-get_topts(lapply(tofit[c(1,2)],function(x)fitarr(x)))
fitted_a$PFT<-names(tofit[c(1,2)])


fitted_params<-rbind.fill(fitted_p,fitted_a)

# arctic<-data.frame(do.call(rbind,list(lapply(tofit[1],function(x)fitpeaked(x,return="Arr"))[[1]]))) # cannot fit Peak model
# arctic$PFT<-"ARCTIC"
# 
# fitted_p<-rbind.fill(fitted_p,arctic)
#-----------------------------------------------------------------------------------------------------------------
#-----------------------------------------------------------------------------------------------------------------

#plot fitted functions


#-----------------------------------------------------------------------------------------------------------------
#-----------------------------------------------------------------------------------------------------------------

fit_parr<-function(Ts,data){
  # Vcmax<-Ropt*((200*exp(Ea*(Tk(Ts)-Tk(Topt))/(Tk(Ts)*0.008314*Tk(Topt)))/(200-(Ea*(1-exp(200*(Tk(Ts)-Tk(Topt))/(Tk(Ts)*0.008314*Tk(Topt))))))))          
  
  Vcmax<-data$Vpmax25 * exp((data$Eap*(Tk(Ts) - 298.15))/(298.15*0.008314*Tk(Ts))) * 
    (1+exp((298.15*data$delsp - 200)/(298.15*0.008314))) / 
    (1+exp((Tk(Ts)*data$delsp-200)/(Tk(Ts)*0.008314)))
  
  return(Vcmax)
}

# function to fit standard arrhenius

fit_arr<-function(Ts,data){
  
  
  # k25<-data$Vpmax25
  # Ea<-data$Eap
  vcmax_or_jmax<- data$Vpmax25 * exp((data$Eap*(Tk(Ts) - 298.15))/(298.15*0.008314*Tk(Ts)))
  return(vcmax_or_jmax)
}

#some functions
.Rgas <- function()8.314
Tk <- function(x)x+273.15


tleaf<-seq(0,50,0.5)

#-----------------------------------------------------------------------------------------------------------------
#-----------------------------------------------------------------------------------------------------------------



BET_Tr<-fit_parr(Ts=tleaf,subset(fitted_params,fitted_params$PFT=="BET_Tr"))
BET_TE<-fit_parr(Ts=tleaf,subset(fitted_params,fitted_params$PFT=="BET_TE"))
NET_B<-fit_parr(Ts=tleaf,subset(fitted_params,fitted_params$PFT=="NET_B"))
NET_TE<-fit_parr(Ts=tleaf,subset(fitted_params,fitted_params$PFT=="NET_TE"))
ARCTIC<-fit_arr(Ts=tleaf,subset(fitted_params,fitted_params$PFT=="ARCTIC"))
BDT_TE<-fit_arr(Ts=tleaf,subset(fitted_params,fitted_params$PFT=="BDT_TE"))



pdf("T-responsecurves.pdf",width = 8, height = 6)
par(mar=c(2,1,0.5,0.5),oma=c(3,3,0,0),mfrow=c(2,3))


# plot Arctic
with(subset(mature_data,mature_data$PFT=="ARCTIC"),plot(Ts,TPU,bg="lightgray",pch=21,cex=2,axes=F,xlab="",ylab="",
                                                        xlim=c(0,30),ylim=c(0,25)))

magaxis(side=c(1,2,4),labels=c(1,1,0),frame.plot=T,las=1,cex.axis=1.1,ratio=0.4,tcl=0.2,majorn = 4)
# title(xlab=expression(T[Leaf]~(degree*C)),cex.lab=1.2,line=2)
title(ylab=expression(V[pmax]~(mu*mol~m^-2~s^-1)),cex.lab=1.5,line=1,outer=T)
lines(tleaf,ARCTIC,type="l",lty=1,lwd=3,col=alpha("black",0.5))
legend("topright",expression((bold(a))),bty="n",cex=1.5)
legend("topleft",expression(bold(Arctic~tundra)),bty="n",cex=1.5)



# plot Boreal 
with(subset(mature_data,mature_data$PFT=="NET_B"),plot(Ts,TPU,bg="lightgray",pch=21,cex=2,axes=F,xlab="",ylab="",
                                                       xlim=c(0,45),ylim=c(0,25)))
magaxis(side=c(1,2,4),labels=c(1,0,0),frame.plot=T,las=1,cex.axis=1.1,ratio=0.4,tcl=0.2,majorn = 4)


lines(tleaf,NET_B,type="l",lty=1,lwd=3,col=alpha("black",0.5))
legend("topright",expression((bold(b))),bty="n",cex=1.5)
legend("topleft",expression(bold(EG-Br)),bty="n",cex=1.5)


# plot Temperate (Eg-Gy) 
with(subset(mature_data,mature_data$PFT=="NET_TE"),plot(Ts,TPU,bg="lightgray",pch=21,cex=2,axes=F,xlab="",ylab="",
                                                        xlim=c(10,40),ylim=c(0,25)))
magaxis(side=c(1,2,4),labels=c(1,0,0),frame.plot=T,las=1,cex.axis=1.1,ratio=0.4,tcl=0.2,majorn = 4)


lines(tleaf,NET_TE,type="l",lty=1,lwd=3,col=alpha("black",0.5))
legend("topright",expression((bold(c))),bty="n",cex=1.5)
legend("topleft",expression(bold(EG-Te)),bty="n",cex=1.5)


# plot Temperate (deciduous-An) 
with(subset(mature_data,mature_data$PFT=="BDT_TE"),plot(Ts,TPU,bg="lightgray",pch=21,cex=2,axes=F,xlab="",ylab="",
                                                        xlim=c(0,35),ylim=c(0,25)))
magaxis(side=c(1,2,4),labels=c(1,1,0),frame.plot=T,las=1,cex.axis=1.1,ratio=0.4,tcl=0.2,majorn = 4)


lines(tleaf,BDT_TE,type="l",lty=1,lwd=3,col=alpha("black",0.5))
legend("topright",expression((bold(d))),bty="n",cex=1.5)
legend("topleft",expression(bold(DA-Te)),bty="n",cex=1.5)

# title(xlab=expression(T[Leaf]~(degree*C)),cex.lab=1.2,line=2)
# title(ylab=expression(TPU~(mu*mol~m^-2~s^-1)),cex.lab=1.5,line=2)


# plot Temperate (evergreen-An)

with(subset(mature_data,mature_data$PFT=="BET_TE"),plot(Ts,TPU,bg="lightgray",pch=21,cex=2,axes=F,xlab="",ylab="",
                                                        xlim=c(10,45),ylim=c(0,25)))
magaxis(side=c(1,2,4),labels=c(1,0,0),frame.plot=T,las=1,cex.axis=1.1,ratio=0.4,tcl=0.2,majorn = 4)


lines(tleaf,BET_TE,type="l",lty=1,lwd=3,col=alpha("black",0.5))
legend("topright",expression((bold(e))),bty="n",cex=1.5)
legend("topleft",expression(bold(EA-Te)),bty="n",cex=1.5)

# title(xlab=expression(T[Leaf]~(degree*C)),cex.lab=1.2,line=2)

# plot Tropical (evergreen-An)

with(subset(mature_data,mature_data$PFT=="BET_Tr"),plot(Ts,TPU,bg="lightgray",pch=21,cex=2,axes=F,xlab="",ylab="",
                                                        xlim=c(15,45),ylim=c(0,25)))
magaxis(side=c(1,2,4),labels=c(1,0,0),frame.plot=T,las=1,cex.axis=1.1,ratio=0.4,tcl=0.2,majorn = 4)


lines(tleaf,BET_Tr,type="l",lty=1,lwd=3,col=alpha("black",0.5))
legend("topright",expression((bold(f))),bty="n",cex=1.5)
legend("topleft",expression(bold(EA-Tr)),bty="n",cex=1.5)
title(xlab=expression(T[Leaf]~(degree*C)),cex.lab=1.5,line=1,outer=T)


dev.off()


#-----------------------------------------------------------------------------------------------------------------
#-----------------------------------------------------------------------------------------------------------------
#------------------------------------------------------------------------------------------------------------
#------------------------------------------------------------------------------------------------------------

# plot 
# get a subset of dataset: (mature trees in native environments) 18 dataset


tfits<-read.csv("aci_fits.csv")

# get curves only show TPU limitation
fits_global_tpu<-subset(tfits,!is.na(TPU)) 

# Get best fitted curves (Best fit if R2>=0.90; My definition)
params_global_clean<-subset(fits_global_tpu, fits_global_tpu$R2_TPU>=0.9)
col_codes<-read.csv("color_codes.csv")
params_global_clean<-merge(params_global_clean,col_codes,by="PFT")



mature_data_new<-subset(params_global_clean,  Dataset %in% c("Semi-arid Woodland, AU-WA","Rainforest, Brazil",
                                                             "Maritime Pine, France","Rainforest, Au-QLD",
                                                             "Scots Pine, Sweden","Tundra USA-AK","Black Spruce, USA-MN","Mongolian Oak, Japan",
                                                             "Savanna Eucalypt, AU-NT","Eucalypt Woodland, AU-NSW",
                                                             "Loblolly Pine, US-NC","Rainforest understorey, Puerto Rico","Rainforest, Panama (A)",
                                                             "Scots Pine, Finland","Subalpine Eucalypt, AU-NSW (A)") & params_global_clean$Ci_t2_tpu>400.0001) 

mature_data_new$Dataset<-factor(mature_data_new$Dataset)




pdf("figure_6_citrans_vs_thome.pdf",width = 6, height = 3)
par(mfrow=c(1,2),mar=c(0.5,0.5,0.5,0.5),oma=c(3,3.5,0,0),cex.axis=1.5,las=1)


# without TPU

mature_data_new<- subset(mature_data_new,mature_data_new$Ts>18 & mature_data_new$Ts<26)

with(mature_data_new,plot(mGDD0,Ci_t1_tpu,bg=alpha(COL,0.9)[mature_data_new$colorcode],pch=mature_data_new$pchcode,cex=0.6,axes=F,xlab="",ylab="",
                          ylim=c(0,2000),col=alpha(COL,0.9)[mature_data_new$colorcode]))
magaxis(side=c(1,2,4),labels=c(1,1,0),frame.plot=T,las=1,cex.axis=1.1,ratio=0.4,tcl=0.2,majorn = 4)

legend("topright",paste("(",letters[1],")",sep=""),bty="n",cex=1,text.font=2)

abline(h=400*0.7,lty=2,lwd=2)

legend("topleft",legend=levels(unique(mature_data_new$PFT)),pt.bg=alpha(COL,1)[c(1:6)],
       pch=c(21,22,24,24,25,25),bty="n",ncol=2,cex=0.6)

abline(h=400*0.7,lty=2,lwd=2)



# with TPU

with(mature_data_new,plot(mGDD0,Ci_t2_tpu,bg=alpha(COL,0.9)[mature_data_new$colorcode],pch=mature_data_new$pchcode,cex=0.6,axes=F,xlab="",ylab="",
                          ylim=c(0,2000),col=alpha(COL,0.9)[mature_data_new$colorcode]))
magaxis(side=c(1,2,4),labels=c(1,0,0),frame.plot=T,las=1,cex.axis=1.1,ratio=0.4,tcl=0.2,majorn = 4)

legend("topright",paste("(",letters[2],")",sep=""),bty="n",cex=1,text.font=2)

title(xlab=expression(mGDD0~(degree*C)),cex.lab=1.2,line=2,outer=T)
title(ylab=expression(C[i]~(mu*mol~mol^-1)),cex.lab=1.2,line=2,outer=T)
abline(h=400*0.7,lty=2,lwd=2)
dev.off()


#---------------------------------------------------------------------------------------------
#---------------------------------------------------------------------------------------------

# analysis of variance 
#TPU at 25C


# anova for ratio
library(multcomp)
lme_1<-lmer(TV_ratio~PFT+(1|Dataset),data=mature_data_new)
Anova(lme_1,test="F")
rsquared.glmm(lme_1)

# posthoc analysis
out.aopt <- glht(lme_1, linfct = mcp(PFT="Tukey"))
out.aopt <- summary(out.aopt)

#---------------------------------------------------------------------------------------------
#---------------------------------------------------------------------------------------------
